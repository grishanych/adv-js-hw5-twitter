class Card {
    constructor(postId, title, text, user) {
        this.postId = postId;
        this.title = title;
        this.text = text;
        this.user = user;
    }

    render() {
        const cardElement = document.createElement('div');
        cardElement.classList.add('card');

        const titleElement = document.createElement('h2');
        titleElement.classList.add('card-title');
        titleElement.textContent = this.title;

        const textElement = document.createElement('p');
        textElement.innerText = this.text;

        const userElement = document.createElement('p');
        userElement.innerHTML = `<strong>Author:</strong> <em>${this.user.name}</em> // <strong>email:</strong> <em>${this.user.email}</em>`;

        const deleteButton = document.createElement('button');
        deleteButton.classList.add('del-btn')
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => this.deleteCard());

        const actionsElement = document.createElement('div');
        actionsElement.classList.add('card-actions');
        actionsElement.append(deleteButton);

        cardElement.append(titleElement);
        cardElement.append(textElement);
        cardElement.append(userElement);
        cardElement.append(actionsElement);

        return cardElement;
    }

    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
            method: 'DELETE'
        })
            .then(response => {
                if (response.ok) {
                    const cardElement = document.getElementById(`card-${this.postId}`);
                    if (cardElement) {
                        cardElement.remove();
                    }
                } else {
                    throw new Error('Failed to delete the card');
                }
            })
            .catch(error => {
                console.error(error);
            });
    }

}

// Отримання списку користувачів та публікацій з сервера
fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(users => {
        fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
            .then(posts => {
                displayNewsFeed(posts, users);
            });
    })
    .catch(error => {
        console.error(error);
    });

// Відображення стрічки новин
function displayNewsFeed(posts, users) {
    const newsFeedElement = document.getElementById('newsFeed');

    posts.forEach(post => {
        const user = users.find(u => u.id === post.userId);
        if (user) {
            const card = new Card(post.id, post.title, post.body, user);
            const cardElement = card.render();
            cardElement.id = `card-${post.id}`;
            newsFeedElement.append(cardElement);
        }
    });
}